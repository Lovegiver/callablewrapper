# CallableWrapper

This is a 'training code' around the use of Callables, ExecutorServices and Generics.<br>

The starting point was the following : when using a ScheduledExecutorService, the number of cores has to be specified and remains the same, whatever the activity level is. So is there a way to deal with that constraint in order to avoid a non-optimized resources allocation ?<br>

This app is a try to find a workaround. HOW ?<br>

By using a kind of 'Trojan Horse' strategy, making the app believe that there is only 1 allocated core and 1 unique Callable to execute where, in reality, this unique Callable is a data structure made of a collection of Callables.<br>

Unfortunately, the ScheduledExecutorService seems to rule the way cores are managed by the whole app. When a single core is allocated, the collection of Callables will be read and the Callables executed one by one, even if the 'Trojan' data structure has its own ExecutoService made of multiple cores.<br>

Nevertheless, this try is a good working one. The ScheduledExecutorService has been replaced by a simple 'while' loop and a SingleThreadExecutor, making our 'Trojan Horse' stratgy working.

Our Trojan Horse is played by a SuperWrapper object that accepts only T-typed objects (single parameter : T extends Callable<?> ). Once a collection of T-typed objects has been passed as argument, an ExecutorService with floating number of cores manages the parallel execution of all this Callables.<br>

Each Callable is 'manageable' : it remains possible to know for each if its execution is Done or not.
